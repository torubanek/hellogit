#include <iostream>
#include <random> 

using namespace std;


int main()
{
    std::random_device rd; // will be used to seed eng1
    std::mt19937 eng1(rd()); // this engine produces different ouput every time
    std::mt19937 eng2(1337);// this engine  produces the same output every time

    float mean = 36.7f; // average
    float sd = 1.55f; // standard deviation

    int min = 10; // minimum number to generate
    int max = 48; // maximum number to generate

    std::normal_distribution<float> norm(mean,sd);
    std::uniform_int_distribution<> unifrm(10,52);

	for(int n=0 ;  n < 6 ;  n++) 
	{
        cout<<norm(eng1)<<" "; // different output each time
    }
    cout<<endl<<endl;

	for(int n=0 ;  n < 6 ; n++) 
	{
        cout<<unifrm(eng2)<<" ";// same output each time
    }
}
    
    
